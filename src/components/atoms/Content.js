import React, {Component} from 'react';
import {Text, StyleSheet, TouchableOpacity, View} from 'react-native';
import CText from '../atoms/CText';

export default class Content extends Component {
  render() {
    const {style} = this.props;
    return (
      <TouchableOpacity {...this.props} style={{...styles.main, ...style}}>
          {this.props.children}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    width: 390,
    marginTop: 20,
    marginHorizontal: 10,
    backgroundColor: '#dfe3ee',
    borderRadius: 15,
    paddingVertical: 10,
    elevation: 10,
  },
  text: {
    paddingLeft: 10,
    fontSize: 20,
    color: 'white',
    fontFamily: 'YesevaOne-Regular',
  },
});
