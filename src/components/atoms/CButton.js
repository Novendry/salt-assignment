import React, {Component} from 'react';
import {Text, StyleSheet, TouchableOpacity, View} from 'react-native';

export default class CButton extends Component {
  render() {
    const {style, text} = this.props;
    return (
      <View>
        <TouchableOpacity {...this.props} style={{...styles.main, ...style}}>
          <Text
            style={{
              ...{fontWeight: '900', color: '#dfe3ee', fontSize: 20},
              ...text,
            }}>
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 3,
    margin: 5,
    backgroundColor: '#3b5998',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    // borderWidth:2,
    // borderColor:'#dfe3ee',
    elevation: 5,
    marginHorizontal: 80,
  },
});
