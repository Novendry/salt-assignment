import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import CText from './CText';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Layout extends Component {
  render() {
    const {style, title, children, iconPress} = this.props;
    return (
      <View {...this.props} style={{...styles.mainContainer, ...style}}>
        <View style={styles.header}>
          <MaterialComunnityIcon
            name="arrow-left"
            size={30}
            color={'#dfe3ee'}
            onPress={iconPress}
          />
          <CText>{title}</CText>
          <View style={{marginRight: 40}} />
        </View>
        <ScrollView>{children}</ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#8b9dc3',
  },
  header: {
    backgroundColor: '#3b5998',
    height: 45,
    elevation: 10,
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
