import React, { Component } from 'react'
import { Text, StyleSheet, TouchableOpacity,View } from 'react-native'

export default class CText extends Component {
    render() {
        const {style}= this.props
        return (
            <View>
               <Text {...this.props} style={{...styles.textDefault,...style}}>{this.props.children}</Text>
           </View>
        )
    }
}

const styles = StyleSheet.create({
    textDefault:{
        fontSize:20,
        color:'#dfe3ee',
        // paddingLeft:20,
        fontFamily: 'YesevaOne-Regular',
    }
})
