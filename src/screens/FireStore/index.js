import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';
import firestore from '@react-native-firebase/firestore';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      name: '',
      email: '',
    };
  }

  componentDidMount() {
    let newData;
    firestore()
      .collection('Users')
      .onSnapshot(v => {
        newData = v.docs.map(res => {
          return res.data();
        });
        newData.length > 0 && this.setState({data: newData});
      });
  }

  _add() {
    const {name, email} = this.state;
    firestore()
      .collection('Users')
      .add({
        name,
        email,
      })
  }

  render() {
    const {data} = this.state;
    console.log(data);
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={styles.output}>
          {data.map((v, i) => {
            return (
              <View style={styles.list} key={i}>
                <Text style={{width: 180}}>{v.name}</Text>
                <Text>{v.email}</Text>
              </View>
            );
          })}
        </View>
        <View style={styles.box}>
          <TextInput
            style={styles.textInput}
            placeholder="Name"
            onChangeText={value => this.setState({name: value})}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Email"
            onChangeText={value => this.setState({email: value})}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={() => this._add()}>
          <Text style={styles.textButton}>Add</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    flexDirection: 'row',
  },
  output: {
    // flexDirection:'row',
    padding:10,
    width: 400,
    height: 200,
    borderWidth: 3,
    borderRadius: 15,
    backgroundColor: '#dfe3ee',
    borderColor: '#3b5998',
    elevation: 10,
  },
  textInput: {
    marginRight: 10,
    marginLeft: 10,
    fontSize: 17,
    fontWeight: '500',
    paddingLeft: 10,
    width: 220,
    color: '#3b5998',
  },
  box: {
    width: 360,
    height: 200,
    backgroundColor: '#dfe3ee',
    borderRadius: 15,
    marginVertical: 15,
    justifyContent: 'space-around',
    paddingVertical: 20,
    elevation: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#3b5998',
    borderRadius: 15,
    marginHorizontal: 50,
    marginVertical: 25,
    paddingVertical: 5,
    elevation: 10,
  },
  textButton: {
    fontSize: 25,
    color: '#dfe3ee',
    fontWeight: '900',
    paddingHorizontal: 10,
  },
});
