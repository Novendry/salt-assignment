import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,20],
      fib:[]
    };
  }

  componentDidMount(){
    const {data}=this.state
    const fib=[...data]
    for (let i = 2; i <= 20; i++) {fib[i] = fib[i - 2] + fib[i - 1]}
    this.setState({fib})
  }

  render() {
    const {data,fib} = this.state;
    console.log(data)
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:"center"}}>
        {fib.map((v,i)=>{
          return <View key={i}><Text>{v}</Text></View>
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
