import React, {Component} from 'react';
import {
  StatusBar,
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
    };
  }

  async getMovies() {
    try {
      const response = await fetch(
        'http://www.omdbapi.com/?apikey=997061b4&s=Lego',
      );
      const json = await response.json();
      this.setState({data: json.Search});
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({isLoading: false});
    }
  }

  componentDidMount() {
    this.getMovies();
  }

  render() {
    const {data, isLoading} = this.state;

    return (
      <View style={styles.mainContainer}>
        <StatusBar hidden />
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('redux');
              //CRUDstatement
            }}>
            <Text style={styles.textHeader}>Movies</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.push('Inbox');
              //FireStrore
            }}>
            <MaterialComunnityIcon name="inbox" size={35} color={'#dfe3ee'} />
          </TouchableOpacity>
        </View>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={styles.content}
                onPress={() => {
                  this.props.navigation.navigate('DetilContent'),
                    {link: item.imdbID};
                }}>
                <View style={styles.posterWrapper}>
                  <Image style={styles.poster} source={{uri: item.Poster}} />
                </View>
                <Text style={styles.textTitle}>{item.Title}</Text>
                <Text style={styles.textYear}>{item.Year}</Text>
              </TouchableOpacity>
            )}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#3b599880',
  },
  header: {
    widht: 200,
    height: 50,
    backgroundColor: '#3b5998',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  textHeader: {
    fontSize: 20,
    color: '#dfe3ee',
    fontFamily: 'Pacifico-Regular'
  },
  textTitle: {
    textAlign: 'center',
    fontSize: 17,
    marginHorizontal: 10,
    fontWeight: 'bold',
    color: '#3b5998',
    fontFamily: 'Pacifico-Regular'
  },
  textYear: {
    fontSize: 15,
    color: '#3b5998',
    paddingBottom: 5,
  },
  posterWrapper: {
    borderBottomWidth: 2,
    width: 380,
    paddingBottom: 5,
    borderColor: '#3b599880',
  },
  poster: {
    width: 396,
    height: 200,
    borderRadius: 10,
    alignSelf: 'center',
  },
  content: {
    alignItems: 'center',
    backgroundColor: '#dfe3ee',
    marginTop: 10,
    marginHorizontal: 8,
    borderRadius: 10,
  },
  cbutton: {
    backgroundColor: 'red',
  },
});
