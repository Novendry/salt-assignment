import {Text, View,ScrollView} from 'react-native';
import React, {Component} from 'react';
import  axios  from 'axios';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount(){
      axios.get('https://jsonplaceholder.typicode.com/users')
      .then(res=>{
          this.setState({users: res.data})
      })
  }

  render() {
    const {users} = this.state; 
    console.log(users)
    return (
      <ScrollView>
          {users.map((v,i)=>{
              return (
                  <View style={{margin:20,padding:10,borderWidth:1}} key={i}>
                      <Text>id:{v.id}</Text>
                      <Text>name:{v.name}</Text>
                      <Text>username:{v.username}</Text>
                      <Text>email:{v.email}</Text>
                      <Text>{v.address.street}</Text>
                      <Text>{v.address.suite}</Text>
                      <Text>{v.address.city}</Text>
                  </View>
              )
          })}
      </ScrollView>
    );
  }
}

