import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _add(){
    firestore()
      .collection('Users')
      .add({
        name: 'Anggara',
        email: 'anggara@gmail.com',
      })
      .then(() => {
        console.log('User added!');
      });
  }

  render() {
    return (
      <View style={styles.main}>
        <StatusBar hidden />
        <Image
          style={styles.logo}
          source={require('../../assets/images/logo.png')}
        />
        <Text style={styles.fontLogo}>Toko Online</Text>
        <Text style={styles.font}>by Mahrus</Text>
        <Text style={styles.header}>Login</Text>
        <View style={styles.input}>
          <MaterialComunnityIcon name="account" size={30} color="black" />
          <TextInput placeholder="Email Address" style={styles.textInput} />
        </View>
        <View style={styles.input}>
          <MaterialComunnityIcon name="key" size={30} color="black" />
          <TextInput
            style={styles.textInput}
            placeholder="Password"
            onChangeText={v=> this.setState({email:v})}></TextInput>
        </View>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.fontButton}>Sign In</Text>
          <MaterialComunnityIcon name="arrow-right" size={30} color="white"/>
        </TouchableOpacity>
        <Text style={styles.fontForgot}>
          Forgot Password?
          <Text style={styles.fontForgot}>
            <Text style={{color: 'red'}}> Get Here</Text>
          </Text>
        </Text>
        <Text style={styles.textSignUp}>
          You are new?
          <Text
            style={styles.textSignUp}
            onPress={() => this.props.navigation.navigate('SignupNew')}>
            <Text style={{color: 'red'}}> Create New!</Text>
          </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    marginTop: 65,
  },
  logo: {
    resizeMode: 'contain',
    width: 100,
    height: 100,
  },
  fontLogo: {
    fontFamily: 'Pacifico-Regular',
    color: '#FF0066',
    fontSize: 24,
  },
  font: {
    fontFamily: 'YesevaOne-Regular',
    color: 'black',
    marginBottom: 50,
  },
  header: {
    fontSize: 36,
    fontWeight: '900',
    color: 'black',
  },
  input: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 15,
    paddingLeft: 10,
    alignItems: 'center',
    marginVertical: 10,
  },
  textInput: {
    width: 290,
    fontSize: 21,
    paddingLeft: 20,
  },
  button: {
    flexDirection:'row',
    backgroundColor: '#FF0066',
    paddingVertical: 5,
    width: 330,
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
  },
  fontButton: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  fontForgot: {
    justifyContent: 'center',
    marginTop:20
  },
});
