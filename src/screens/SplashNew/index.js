import React, {Component} from 'react';
import {Image, StyleSheet, Text, View, StatusBar} from 'react-native';

export default class index extends Component {

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('LoginNew');
    }, 3000);
  }

  render() {
    return (
    <View style={styles.main}>
        <StatusBar hidden/>
        <Image source={require('../../assets/images/logo.png')}/>
        <Text style={styles.fontLogo} >Toko Online</Text>
        <Text style={styles.font} >by Mahrus</Text>
        <Image style={styles.images} source={require('../../assets/images/splashbot.png')}/>
    </View>
    );
  }
}

const styles = StyleSheet.create({
    main:{
        flex:1,
        justifyContent:'flex-end',
        alignItems:'center'
    },
    fontLogo:{
        fontFamily:'Pacifico-Regular',
        color:'#FF0066',
        fontSize:24,
    },
    font:{
        fontFamily:'YesevaOne-Regular',
        marginBottom:100,
        color:'black'
    },
    images:{
        width:411,
        height:150
    }
});