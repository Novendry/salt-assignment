import {View, StyleSheet, Image, Text} from 'react-native';
import React, {Component} from 'react';
import Content from '../../components/atoms/Content';
import Layout from '../../components/atoms/Layout';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class index extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const {data} = this.props.route.params;
    // console.log(data);
    return (
      <Layout
        title={data.title}
        iconPress={() => this.props.navigation.goBack()}>
        <Content style={styles.contentWrapper}>
          {data.imageUrl ? (
            <Image style={styles.image} source={{uri: data.imageUrl}} />
          ) : null}
          <Text style={styles.text}>{data.body}</Text>
        </Content>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    height: 260,
    width: 360,
    alignSelf: 'center',
    borderRadius: 15,
    borderWidth: 2,
    borderColor: '#3b5998',
    marginVertical: 10,
  },
  contentWrapper: {
    borderTopStartRadius: 0,
  },
  text: {
    color: '#3b5998',
    marginHorizontal: 25,
    fontSize: 17,
    textAlign: 'justify',
  },
  arrowIcon: {
    margin: 10,
    marginBottom: 0,
  },
});
