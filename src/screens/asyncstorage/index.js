import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  async componentDidMount() {
    this._saveData();
    this._getData();
  }

  _saveData = async () => {
    const stringValue = 'Cuma ngetes gaes';
    const objectValue = {
      name: 'Novendry',
      address: 'Bikini Bottom',
    };
    const arrayValue = [1, 2, 3, 4];
    const convertToString = JSON.stringify(objectValue);
    try {
      await AsyncStorage.setItem('tes', convertToString);
    } catch (e) {
      console.log(e);
    }
    console.log(convertToString);
  };

  _getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('tes');
      return (
        jsonValue != null && this.setState({data: [JSON.parse(jsonValue)]})
      );
    } catch (e) {}
  };

  render() {
    const {data} = this.state;
    console.log(data);
    return (
      <View style={styles.container}>
        {data.map((v, i) => {
          return (
            <View key={i}>
              <Text style={styles.font}>name: {v.name}</Text>
              <Text style={styles.font}>address: {v.address}</Text>
            </View>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex:1,
      backgroundColor:'#8b9dc3',
      justifyContent:'center',
      alignItems:'center'
  },
  font: {
    fontSize: 25,
    fontFamily: 'Pacifico-Regular',
    color: '#3b5998',
  },
});
