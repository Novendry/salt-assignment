import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import CButton from '../../components/atoms/CButton';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      ],
    };
  }

 
  render() {
    return (
      /*bilangan ganjil*/
      <View
        style={{flex: 1, justifyContent: 'space-evenly'}}>
        <CButton title='Bilangan Ganjil' onPress={() => this.props.navigation.navigate('BilanganGanjil')}/>
        <CButton title='Bilangan Genap' onPress={() => this.props.navigation.navigate('BilanganGenap')}/>
        <CButton title='Bilangan Prima' onPress={() => this.props.navigation.navigate('BilanganPrima')}/>
        <CButton title='Bilangan Fibonacci' onPress={() => this.props.navigation.navigate('BilanganFibonacci')}/>
        <CButton title='Tampilan Post' onPress={() => this.props.navigation.navigate('TampilanPost')}/>
        <CButton title='Tampilan Users' onPress={() => this.props.navigation.navigate('TampilanUsers')}/>
        <CButton title='Tampilan Anastasya.net' onPress={() => this.props.navigation.navigate('TampilanUsersAnas')}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
