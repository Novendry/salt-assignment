import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
  TouchableOpacity,Icon
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather'
import CButton from '../../components/atoms/CButton';
import CText from '../../components/atoms/CText'

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //   name: '',
      //   address:'',
      data: {
        name: 'Novendry',
        address: 'Jogja',
      },
      table: [
        {id: 1, name: 'Novendry', address: 'BikiniBottom'},
        {id: 2, name: 'Anggara', address: 'BikiniTop'},
        {id: 3, name: 'Putra', address: 'BikiniRight'},
        {id: 4, name: 'Putri', address: 'BikiniLeft'},
      ],
    };
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        data: {
          name: 'Bambang',
          address: 'Jombang',
        },
      });
    }, 3000);
  }

  _addData() {
    const {id, name, address,table} = this.state;
    
    this.setState({
      table: [...table,{id,name,address}],
      id:'',
      name:'',
      address:''
    });
  }
  _button() {
    const {name, address} = this.state;
    this.setState({
      data: {
        name: name,
        address: address,
      },
    });
  }

  _submit() {
    const {name, address} = this.state;
    this.setState({
      data: {
        name: name,
        address: address,
      },
    });
  }
  render() {
    const {data, name, address, table,id} = this.state;

    return (
      // <View style={{flex: 1}}>
      //   <Text style={{textAlign: 'center', marginVertical: 10}}>Statement</Text>
      //   <View style={styles.card}>
      //     <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
      //       <Text style={{fontWeight: 'bold'}}>Nama</Text>
      //       <Text style={{fontWeight: 'bold'}}>Alamat</Text>
      //     </View>
      //     <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
      //       <Text>{data.name}</Text>
      //       <Text>{data.address}</Text>
      //     </View>
      //     <View style={styles.textInput}>
      //       <TextInput
      //         onChangeText={input => {
      //           this.setState({name: input});
      //         }}
      //         placeholder='enter name'
      //       />
      //       <TextInput
      //         onChangeText={input => {
      //           this.setState({address: input});
      //         }}
      //         placeholder='enter address'
      //       />
      //     </View>
      //     <Button
      //       title='Change State'
      //       onPress={() => {
      //         this._button();
      //       }}
      //     />
      //   </View>
      <View style={{height:500,width:410}}>
        <View style={styles.card2}>
          <View style={styles.tableHeader}>
            <Text style={{width: 30, marginHorizontal: 5}}>ID</Text>
            <Text style={{width: 150, marginHorizontal: 5}}>Name</Text>
            <Text style={{width: 100, marginHorizontal: 5}}>Address</Text>
          </View>
          {table.map((v, i) => {
            return (
              <View key={i} style={styles.tableHeader}>
                <Text style={{width: 30, marginHorizontal: 5}}>{v.id}</Text>
                <Text style={{width: 150, marginHorizontal: 5}}>{v.name}</Text>
                <Text style={{width: 100, marginHorizontal: 5}}>{v.address}
                </Text>
              </View>
            );
          })}
        </View>
        <View style={styles.input}>
          <TextInput style={styles.ti}
            keyboardType='number-pad'
            placeholder="ID" value={id}
            onChangeText={input => {
              this.setState({id: input});
            }}
          />
          <TextInput style={styles.ti}
            placeholder="Name" value={name}
            onChangeText={input => {
              this.setState({name: input});
            }}
          />
          <TextInput style={styles.ti}
            placeholder="Addres" value={address}
            onChangeText={input => {
              this.setState({address: input});
            }}
          />
        </View>
        <View style={{flex:1, alignItems:'center'}}>
        <CButton
          title="Add Data"
          onPress={() => {
            this._addData();
          }}
        />
        <CText>test</CText>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 5,
  },
  textInput: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 15,
  },
  card2: {
    backgroundColor:'#3b599880',
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 5,
  },
  input:{
    justifyContent: 'space-evenly', 
    flexDirection: 'row',
    backgroundColor:'#3b599880',
    marginTop:30,
  },
  tableHeader: {
    flexDirection: 'row',
  },
  ti:{
    backgroundColor:'#3b599880',
    width:100,
    marginVertical:10,
    borderRadius:20,
    fontSize:15,
    fontWeight:'bold',
    fontStyle:'italic',
    textAlign:'center'
  }
});
