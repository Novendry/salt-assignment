import React, {Component} from 'react';
import {Text, StyleSheet, View, ScrollView} from 'react-native';
import axios from 'axios';

export default class userData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    axios.get('https://dummyjson.com/users').then(response => {
      this.setState(response.data);
    });
  }

  render() {
    const {users} = this.state;
    return (
      <View style={{flex: 1}}>
        {users &&
          users.map((v, i) => {
            return (
              <View style={styles.main} key={i}>
                <View style={styles.content}>
                  <Text style={styles.text}>id: {v.id}</Text>
                  <Text style={styles.text}>first name: {v.firstName}</Text>
                  <Text style={styles.text}>last name: {v.lastName}</Text>
                </View>
              </View>
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#3b5998',
  },
  content: {
    backgroundColor: 'white',
    margin: 10,
    // alignItems:'center',
    borderRadius: 10,
  },
  text: {
    color: '#3b5998',
    margin: 5,
    fontSize: 20,
  },
});
