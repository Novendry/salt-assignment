import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import axios from 'axios';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: [],
    };
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/posts').then(res => {
      this.setState({post: res.data});
    });
  }

  render() {
    const {post} = this.state;
    console.log(post);
    return (
      <ScrollView style={{flex: 1}}>
        {post.map((v, i) => {
          return (
            <View key={i} style={styles.post}>
              <Text style={styles.title}>{v.userId}</Text>
              <Text style={styles.title}>{v.title}</Text>
              <Text style={styles.body}>{v.body}</Text>
            </View>
          );
        })}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  post: {
    padding: 20,
    alignContent: 'center',
    margin:10,
    borderWidth:1
  },
  title: {
    color: 'red',
  },
  body: {
    borderWidth:1,
    padding:5
  },
});
