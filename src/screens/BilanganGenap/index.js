import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      ],
    };
  }

 
  render() {
    const {data} = this.state; 
    console.log(data)
    return (
      <View
        style={{flex: 1, flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <View>
          <Text>Genap</Text>
          {data.map((v, i) => {
            return <View key={i}>{v % 2 == 0 && <Text>{v}</Text>}</View>;
          })}
        </View>
       </View>
    );
  }
}

const styles = StyleSheet.create({});
