import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import axios from 'axios';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
      ],
    };
  }

  componentDidMount(){
    let tampung =[]
    for (let index = 0; index < 1000; index++) {
      tampung.push(index)
    }
    this.setState({data: tampung})
  }

 
  render() {
    const {data} = this.state; 
    console.log(data)
    return (
      <View
        style={{flex: 1}}>
          {data.map((v, i) => {
            return (
              <View key={i}>
                <View>{v>=3 && v%3 !=0 && v%2 !=0 && v%5 !=0 && <Text>{v}</Text>}</View>
                <View>{v<=5 && v !=1 && v!=4 && <Text>{v}</Text>}</View>
              </View>
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});