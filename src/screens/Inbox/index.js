import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import Layout from '../../components/atoms/Layout';
import CText from '../../components/atoms/CText';
import Content from '../../components/atoms/Content';
import {connect} from 'react-redux';
export class Inbox extends Component {
  constructor() {
    super();
    this.state = {
      data:[]
    };
  }

  _deleteInbox() {
    this.props.deleteInbox();
  }

  render() {
    // const {data} = this.state;
    const {navigation, inbox} = this.props;
    console.log('renderInbox',inbox);
    return (
      <Layout title={'Notification'} iconPress={() => navigation.goBack()}>
        {inbox.length > 0 &&
          inbox.map((v, i) => {
            return (
              <Content
                key={i}
                onPress={() =>
                  navigation.push('Detail', {
                    data: {title: v.title, body: v.body, imageUrl: v.imageUrl},
                  })
                }>
                <CText style={styles.text}>{v.title}</CText>
              </Content>
            );
          })}
        <TouchableOpacity
          style={styles.buttonDelete}
          onPress={() => this._deleteInbox()}>
          <Text style={styles.textButton}>Delete All</Text>
        </TouchableOpacity>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  const {inbox} = state;
  return {inbox};
};

const mapDispatchToProps = dispatch => {
  return {
    deleteInbox: data => {
      dispatch({
        type: 'DELETE-INBOX',
        payload: data,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Inbox);

const styles = StyleSheet.create({
  text: {
    color: '#3b5998',
    paddingHorizontal: 18,
  },
  delete: {
    color: 'red',
    alignSelf: 'flex-end',
  },
  buttonDelete: {
    backgroundColor: 'red',
    height: 30,
    width: 100,
    justifyContent: 'center',
    alignSelf: 'center',
    marginVertical: 20,
    borderRadius: 10,
  },
  textButton: {
    color: '#dfe3ee',
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
