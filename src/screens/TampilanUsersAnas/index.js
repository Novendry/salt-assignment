import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import axios from 'axios';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
    };
  }

 componentDidMount(){
     axios.get('https://jsonplaceholder.typicode.com/users')
     .then(res=>{
         this.setState({user: res.data})
     })
 }

  render() {
    const {user} = this.state; 
    console.log(user)
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
        {user.map((v,i)=>{
            if(v.website=='anastasia.net')
            return <View key={i}>
              <Text>name: {v.name}</Text>
              <Text>website: {v.website}</Text>
              <Text>email: {v.email}</Text>
              <Text>phone: {v.phone}</Text>
              </View>
        })}
        </View>
    );
  }
}

const styles = StyleSheet.create({});
