import React, {Component} from 'react';
import {Image, ActivityIndicator, Text, View, StatusBar} from 'react-native';

export default class index extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('LoginScreen');
    }, 3000);
  }
  
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#dfe3ee',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <StatusBar hidden />
        <Image
          source={require('../../assets/images/splash.png')}
          style={{
            resizeMode: 'contain',
            height: 150,
            width: 150,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 40,
          }}
        />
        <ActivityIndicator size="large" color="#3b5998bf" />
      </View>
    );
  }
}
