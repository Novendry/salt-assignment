import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.main}>
        <StatusBar hidden />
        <Image
          style={styles.logo}
          source={require('../../assets/images/logo.png')}
        />
        <Text style={styles.header}>Sign Up</Text>
        <Text style={styles.font}>You have chance to create new account if you really want to.</Text>
        <View style={styles.input}>
          <MaterialComunnityIcon name="account" size={30} color="black" />
          <TextInput placeholder="Full Name" style={styles.textInput} />
        </View>
        <View style={styles.input}>
          <MaterialComunnityIcon name="email" size={30} color="black" />
          <TextInput
            style={styles.textInput}
            placeholder="Email Address"></TextInput>
        </View>
        <View style={styles.input}>
          <MaterialComunnityIcon name="key" size={30} color="black" />
          <TextInput
            style={styles.textInput}
            placeholder="Password"></TextInput>
        </View>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.fontButton}>Sign Up</Text>
          <MaterialComunnityIcon name="arrow-right" size={30} color="white"/>
        </TouchableOpacity>
        <Text style={styles.fontCreateNew}>
          You are new?
          <Text
            style={styles.fontCreateNew}
            onPress={() => this.props.navigation.navigate('LoginNew')}>
            <Text style={{color: 'red'}}> Go Here!</Text>
          </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    marginTop: 65,
  },
  logo: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    alignSelf:'flex-start',
    marginLeft:35
  },
  header: {
    fontSize: 36,
    fontWeight: '900',
    color: 'black',
    alignSelf:'flex-start',
    marginLeft:35
  },
  font: {
    color: 'black',
    fontSize:18,
    marginBottom: 30,
    width:350,
    alignSelf:'flex-start',
    marginLeft:35
  },
  input: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 15,
    paddingLeft: 10,
    alignItems: 'center',
    marginVertical: 10,
  },
  textInput: {
    width: 290,
    fontSize: 21,
    paddingLeft: 20,
  },
  button: {
    flexDirection:'row',
    backgroundColor: '#FF0066',
    paddingVertical: 5,
    width: 330,
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
  },
  fontButton: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  fontCreateNew: {
    justifyContent: 'center',
    marginTop:20
  },
});
