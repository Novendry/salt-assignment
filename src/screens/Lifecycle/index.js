import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';

export default class index extends Component {
  constructor() {
    super();
    this.state = {};
    console.log('constructor');
  }

  componentDidMount() {
    console.log('didmount');
  }

  componentWillUnmount() {
    console.log('willunmount');
  }
  componentDidUpdate() {
    console.log('didupdate');
  }

  render() {
    console.log('render');
    return (
      <View>
        <Text> textInComponent </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
