import React, {Component} from 'react';
import {Text, StyleSheet, View, TextInput} from 'react-native';
import CText from '../../components/atoms/CText';
import CButton from '../../components/atoms/CButton';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {id: 1, name: 'Novendry', address: 'BikiniBottom'},
        {id: 2, name: 'Anggara', address: 'BikiniTop'},
        {id: 3, name: 'Putra', address: 'BikiniRight'},
        {id: 4, name: 'Putri', address: 'BikiniLeft'},
      ],
      name: '',
      address: '',
    };
  }

  addData() {
    const {id, name, address, data} = this.state;

    this.setState({
      data: [...data, {name, address}],
      name: '',
      address: '',
    });
  }

  delete(id) {
    const {data} = this.state;
    const nData = data.filter(v => v.id !== id);
    this.setState({data: nData});
  }

  update(id) {
    const {data, name, address} = this.state;
    const update = data.filter(v => v.id !== id);
    this.setState({
      data: [...update, {...update, name, address}],
    });
  }

  render() {
    const {data, address, name} = this.state;
    return (
      <View style={{height: 700, width: 410, backgroundColor: '#dfe3ee'}}>
        <View style={styles.card2}>
          <View style={styles.tableHeader}>
            <Text
              style={{
                fontSize: 17,
                color: '#3b5998',
                fontWeight: 'bold',
                width: 20,
                marginHorizontal: 5,
              }}>
              ID
            </Text>
            <Text
              style={{
                fontSize: 17,
                color: '#3b5998',
                fontWeight: 'bold',
                width: 100,
                fontSize: 20,
                marginHorizontal: 5,
              }}>
              Name
            </Text>
            <Text
              style={{
                fontSize: 17,
                color: '#3b5998',
                fontWeight: 'bold',
                width: 100,
                marginHorizontal: 5,
              }}>
              Address
            </Text>
          </View>
          {data.map((v, i) => {
            return (
              <View key={i} style={styles.tableHeader}>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#3b5998',
                    fontWeight: 'bold',
                    width: 20,
                    marginHorizontal: 5,
                  }}>
                  {i + 1}
                </Text>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#3b5998',
                    fontWeight: 'bold',
                    width: 100,
                    marginHorizontal: 5,
                  }}>
                  {v.name}
                </Text>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#3b5998',
                    fontWeight: 'bold',
                    width: 100,
                    marginHorizontal: 5,
                  }}>
                  {v.address}
                </Text>
                <MaterialComunnityIcon
                  style={{paddingHorizontal: 20}}
                  name="cancel"
                  color="crimson"
                  size={20}
                  title="Delete"
                  onPress={() => {
                    this.delete(v.id);
                  }}
                />
                <MaterialComunnityIcon
                  style={{paddingHorizontal: 20}}
                  name="autorenew"
                  color="darkgreen"
                  size={20}
                  title="Delete"
                  onPress={() => {
                    this.update(v.id);
                  }}
                />
              </View>
            );
          })}
          <View style={{alignItems: 'center'}}>
            <CButton
              style={{width: 100}}
              title="Add"
              onPress={() => {
                this.addData();
              }}
            />
          </View>
        </View>
        <View style={styles.input}>
          <TextInput
            style={styles.ti}
            placeholder="Name"
            value={name}
            onChangeText={input => {
              this.setState({name: input});
            }}
          />
          <TextInput
            style={styles.ti}
            placeholder="Addres"
            value={address}
            onChangeText={input => {
              this.setState({address: input});
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 15,
  },
  card2: {
    backgroundColor: '#3b599880',
    padding: 10,
    borderRadius: 20,
    borderWidth: 5,
    borderColor: '#3b5998',
    margin: 5,
    marginTop: 50,
  },
  input: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    backgroundColor: '#3b599880',
    marginTop: 30,
  },
  tableHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#3b5998',
  },
  ti: {
    backgroundColor: '#3b599880',
    width: 150,
    marginVertical: 10,
    borderRadius: 20,
    fontSize: 15,
    fontWeight: 'bold',
    color: '#dfe3ee',
    textAlign: 'center',
    borderWidth: 3,
    borderColor: '#3b5998',
  },
});
