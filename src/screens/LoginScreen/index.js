import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import foto from '../../assets/images/background.png';
import CText from '../../components/atoms/CText';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    axios
      .get('https://dummyjson.com/users')
      .then(response => {
        this.setState({users: response.data.users});
      })
      .catch(error => {
        console.log(error);
      });
  }

  onLogin = () => {
    this.props.navigation.navigate('Home');
    //   const {name, pass, users} = this.state;
    //   const validation = users.filter(value => {
    //     return value.username == name && value.password == pass;
    //   });
    //   {
    //     validation.length > 0
    //       ? this.props.navigation.navigate('Inbox')
    //       : alert('Incorrect Username or Password!');
    //   }
    //   console.log(validation);
  };

  render() {
    return (
      <View style={styles.container}>
        <Image source={foto} style={styles.image} />
        <StatusBar hidden />
        <View style={styles.boxOut}>
          <Text style={styles.textHead}>Welcome!</Text>
          <View style={styles.boxIn}>
            <View style={styles.textInputLine}>
              <MaterialComunnityIcon
                name="account"
                size={30}
                color="#3b5998bf"
              />
              <TextInput
                style={styles.textInput}
                placeholder="Username"
                onChangeText={value => this.setState({name: value})}
              />
            </View>
            <View style={styles.textInputLine}>
              <MaterialComunnityIcon name="key" size={30} color="#3b5998bf" />
              <TextInput
                style={styles.textInput}
                placeholder="Password"
                secureTextEntry={true}
                onChangeText={value => this.setState({pass: value})}
              />
            </View>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.onLogin()}>
            <Text style={styles.textButton}>Login</Text>
          </TouchableOpacity>
          <Text style={styles.textSignUp}>
            Don't have an account?
            <Text
              style={styles.textSignUp}
              onPress={() => this.props.navigation.navigate('SignUp')}>
              <Text style={{color: 'red', fontWeight: 'bold'}}> Sign Up!</Text>
            </Text>
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: 412,
    height: 295,
    marginBottom: -50,
  },
  textHead: {
    fontSize: 30,
    color: '#f7f7f7',
    paddingVertical: 20,
    alignSelf: 'center',
    fontFamily: 'Pacifico-Regular',
  },
  boxOut: {
    width: 412,
    height: 440,
    borderTopEndRadius: 60,
    borderTopStartRadius: 60,
    backgroundColor: '#3b5998',
    justifyContent: 'center',
  },
  boxIn: {
    width: 360,
    height: 200,
    backgroundColor: '#dfe3ee',
    borderRadius: 15,
    marginLeft: 25,
    justifyContent: 'space-around',
    paddingVertical: 20,
    elevation: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#dfe3ee',
    borderRadius: 15,
    marginHorizontal: 50,
    marginVertical: 25,
    paddingVertical: 5,
    elevation: 10,
  },
  textButton: {
    fontSize: 25,
    color: '#3b5998',
    fontWeight: '900',
  },
  textInput: {
    marginRight: 10,
    marginLeft: 10,
    fontSize: 19,
    fontWeight: '500',
    paddingLeft: 10,
    width: 220,
    color: '#3b5998',
  },
  textSignUp: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 15,
  },
  textInputLine: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#8b9dc3',
    borderBottomWidth: 2,
    marginHorizontal: 25,
  },
});
