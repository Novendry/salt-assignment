import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import auth from '@react-native-firebase/auth';

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      secure: true,
      email:'',
      password:'',
      currentUser:[]
    };
  }

  submit = () => {
    const {email, password} = this.state;
      auth()
      .createUserWithEmailAndPassword(email,password)
      .then(()=>{
        console.log('User account created & signed in!')
      })
      .catch(error=>{
        if(error.code==='auth/email-already-in-use'){
          console.log('That email address is ready in use!')
        }
        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }
        console.error(error);
      })
  };

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/background.png')}
          style={styles.image}
        />
        <View style={styles.boxOut}>
          <Text style={styles.textHead}>Sign Up</Text>
          <View style={styles.boxIn}>
            <TextInput
              style={styles.textInput}
              keyboardType="email-address"
              placeholder="  Email"
              onChangeText={value => this.setState({email: value})}
            />
            <TextInput
              style={styles.textInput}
              placeholder="  Password"
              secureTextEntry={true}
              onChangeText={value => this.setState({password: value})}
            />
          </View>
          <TouchableOpacity style={styles.button} onPress={() => this.submit()}>
            <Text style={styles.textButton}>Register</Text>
          </TouchableOpacity>
          <Text style={styles.textSignUp}>
            Already have account?
            <Text
              style={styles.textSignUp}
              onPress={() => this.props.navigation.navigate('Home')}>
              <Text style={{color: 'red', fontWeight: 'bold'}}> Login!</Text>
            </Text>
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: 412,
    height: 295,
    marginBottom: -50,
  },
  textHead: {
    fontFamily: 'Pacifico-Regular',
    fontSize: 30,
    color: '#f7f7f7',
    paddingVertical: 20,
    alignSelf: 'center',
  },
  boxOut: {
    width: 412,
    height: 440,
    borderTopEndRadius: 60,
    borderTopStartRadius: 60,
    backgroundColor: '#3b5998',
    justifyContent: 'center',
  },
  boxIn: {
    width: 360,
    height: 200,
    backgroundColor: '#dfe3ee',
    borderRadius: 15,
    marginLeft: 25,
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#dfe3ee',
    borderRadius: 15,
    marginHorizontal: 50,
    marginVertical: 25,
    paddingVertical: 5,
  },
  textButton: {
    fontSize: 25,
    color: '#3b5998',
    fontWeight: '900',
  },
  textInput: {
    marginHorizontal: 30,
    fontSize: 17,
    borderColor: '#8b9dc3',
    borderBottomWidth: 2,
    fontWeight: '500',
    paddingLeft: 10,
  },
  textSignUp: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 15,
  },
});
