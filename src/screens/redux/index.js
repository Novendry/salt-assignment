import React, {Component} from 'react';
import {Text, StyleSheet, View, TextInput} from 'react-native';
import {connect} from 'react-redux';
import CButton from '../../components/atoms/CButton';
import CText from '../../components/atoms/CText'
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      name: '',
      address: '',
      id: '',
    };
  }

  addData() {
    const {id, name, address} = this.state;
    const data = {name, address, id};
    this.props.add(data);
    this.setState({
      name: '',
      address: '',
      id: '',
    });
  }

  delete (id) {
    this.props.delete(id);
  };

  _edit () {
    const {name,address,id} = this.state
    const {students,edit}= this.props
    const data = {id,name,address}
    const newStudent = students.map (v=>{
      if (v.id == data.id){
        (v.id = data.id),
        (v.name = data.name),
        (v.address = data.address)
        return v
      }
      else{
        return v
      }
    })
    edit(newStudent)
    this.setState({
      name:'',
      address:'',
      id:''
    })
  }


  render() {
    const {name, address,id} = this.state;
    const {students,nameTest} = this.props;
    console.log(this.props)
    return (
      <View
        style={{flex: 1, justifyContent: 'center', backgroundColor: '#8b9dc3'}}>
        <View><CText>{nameTest}</CText></View>
        <View style={styles.main}>
          {students.map((v, i) => {
            return (
              <View key={i} style={styles.tableHeader}>
                <Text style={{...styles.stext, width: 20}}>{v.id}</Text>
                <Text style={styles.stext}>{v.name}</Text>
                <Text style={styles.stext}>{v.address}</Text>
                <MaterialComunnityIcon
                  style={{paddingHorizontal: 12}}
                  name="cancel"
                  color="crimson"
                  size={20}
                  onPress={() => {
                    this.delete(v.id);
                  }}
                />
                <MaterialComunnityIcon
                  style={{paddingHorizontal: 12}}
                  name="autorenew"
                  color="darkgreen"
                  size={20}
                  onPress={() => {
                    this._edit(v.id);
                  }}
                />
              </View>
            );
          })}
        </View>
        <View style={styles.input}>
          <TextInput
            style={{...styles.ti, width: 75}}
            placeholder="id"
            value={id}
            onChangeText={input => {
              this.setState({id: input});
            }}
          />
          <TextInput
            style={styles.ti}
            placeholder="Name"
            value={name}
            onChangeText={input => {
              this.setState({name: input});
            }}
          />
          <TextInput
            style={styles.ti}
            placeholder="Addres"
            value={address}
            onChangeText={input => {
              this.setState({address: input});
            }}
          />
        </View>
        <CButton
          title="Add"
          onPress={() => {
            this.addData();
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    students: state.students,
    nameTest: state.nameTest
  };
};

const mapDispatchToProps = dispatch => {
  return {
    add: data => {
      dispatch({
        type: 'ADD-STUDENT',
        payload: data,
      });
    },
    delete: data => {
      dispatch({
        type: 'DELETE-STUDENT',
        payload: data,
      });
    },

    edit: data =>{
      dispatch({
        type: 'EDIT-DATA',
        payload: data
      })
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  main: {
    marginHorizontal: 10,
    borderWidth: 3,
    borderRadius: 15,
    padding: 10,
    borderColor: '#3b5998',
    backgroundColor: '#dfe3ee',
  },
  tableHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#3b5998',
  },
  stext: {
    fontSize: 17,
    color: '#3b5998',
    fontWeight: 'bold',
    width: 110,
    marginHorizontal: 5,
  },
  ti: {
    backgroundColor: '#3b599880',
    width: 150,
    marginVertical: 10,
    borderRadius: 20,
    fontSize: 15,
    fontWeight: 'bold',
    color: '#dfe3ee',
    textAlign: 'center',
    borderWidth: 3,
    borderColor: '#3b5998',
  },
  input: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    backgroundColor: '#3b599880',
    marginTop: 30,
  },
});
