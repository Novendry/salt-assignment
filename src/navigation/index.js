import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screens/Home';
import LoginScreen from '../screens/LoginScreen';
import SignUp from '../screens/SignUp';
import DetilContent from '../screens/DetilContent';
import Statement from '../screens/Statement';
import userData from '../screens/userData';
import CURDstatement from '../screens/CURDstatement';
import Lifecycle from '../screens/Lifecycle';
import Splash from '../screens/Splash';
import redux from '../screens/redux';
import BilanganGanjil from '../screens/BilanganGanjil'
import BilanganGenap from '../screens/BilanganGenap'
import BilanganPrima from '../screens/BilanganPrima'
import BilanganFibonacci from '../screens/BilanganFibonacci'
import TampilanPost from '../screens/TampilanPost'
import TampilanUsers from '../screens/TampilanUsers'
import TampilanUsersAnas from '../screens/TampilanUsersAnas'
import FireStore from '../screens/FireStore'
import LoginNew from '../screens/LoginNew'
import SplashNew from '../screens/SplashNew'
import SignupNew from '../screens/SignupNew'
import asyncstorage from '../screens/asyncstorage'
import Inbox from '../screens/Inbox'
import Detail from '../screens/Detail'
import Layout from '../components/atoms/Layout'

const Stack = createNativeStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Splash">
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="DetilContent" component={DetilContent} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="Statement" component={Statement} />
        <Stack.Screen name="userData" component={userData} />
        <Stack.Screen name="CURDstatement" component={CURDstatement} />
        <Stack.Screen name="Lifecycle" component={Lifecycle} />
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="redux" component={redux} />
        <Stack.Screen name="BilanganGanjil" component={BilanganGanjil} />
        <Stack.Screen name="BilanganGenap" component={BilanganGenap} />
        <Stack.Screen name="BilanganPrima" component={BilanganPrima} />
        <Stack.Screen name="BilanganFibonacci" component={BilanganFibonacci} />
        <Stack.Screen name="TampilanPost" component={TampilanPost} />
        <Stack.Screen name="TampilanUsers" component={TampilanUsers} />
        <Stack.Screen name="TampilanUsersAnas" component={TampilanUsersAnas} />
        <Stack.Screen name="FireStore" component={FireStore} />
        {/* new screen */}
        <Stack.Screen name="SplashNew" component={SplashNew} />
        <Stack.Screen name="LoginNew" component={LoginNew} />
        <Stack.Screen name="SignupNew" component={SignupNew} />
        <Stack.Screen name="asyncstorage" component={asyncstorage} />
        <Stack.Screen name="Inbox" component={Inbox} />
        <Stack.Screen name="Detail" component={Detail} />
        <Stack.Screen name="Layout" component={Layout} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;