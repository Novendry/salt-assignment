const initialState = {
  nameTest: 'tes',
  students: [
    {id: 1, name: 'Novendry', address: 'BikiniBottom'},
    {id: 2, name: 'Anggara', address: 'BikiniTop'},
    {id: 3, name: 'Putra', address: 'BikiniRight'},
    {id: 4, name: 'Putri', address: 'BikiniLeft'},
  ],
  inbox: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD-STUDENT':
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    case 'DELETE-STUDENT':
      return {
        ...state,
        students: state.students.filter(value => {
          return value.id != action.payload;
        }),
      };
    case 'EDIT-DATA':
      return {
        ...state,
        student: action.payload,
      };
    case 'INBOX':
      return {
        ...state,
        inbox: [action.payload, ...state.inbox],
      };
    case 'DELETE-INBOX':
      return {
        ...state,
        inbox: [],
      };
    default:
      return state;
  }
};

export default reducer;
