import React, {Component} from 'react';
import Navigation from './src/navigation/index';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {Persistor, Store} from './src/redux/store';
import messaging from '@react-native-firebase/messaging';

messaging().onMessage(async remoteMessage => {
  console.log('remotemssg',remoteMessage)
  Store.dispatch({
    type: 'INBOX',
    payload: {
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      imageUrl: remoteMessage.notification.android.imageUrl,
    },
  });
});

messaging().setBackgroundMessageHandler(async remoteMessage => {
  Store.dispatch({
    type: 'INBOX',
    payload: {
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      imageUrl: remoteMessage.notification.android.imageUrl,
    },
  });
});



const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
